var app = require('express')();
    server = require('http').createServer(app);

const util = require('util')
const dotenv = require('dotenv');
      dotenv.config();

const PORT = process.env.PORT;
      STYLE = process.env.MESSAGE_STYLE;


  app.use(function middleware(req, res, next) {
    // Do something
    // Call the next function in line:
    var string = req.method + " " + req.path + " - " + req.ip;
    console.log(string);
    next();
  });

app.get("/json", (req, res) => {
  var response = "";

  console.log(response);

  if (process.env.MESSAGE_STYLE == "uppercase") {
    response = "Hello World".toUpperCase();
  } else {
    response = "Hello World";
  }

  res.json({
    message: response
  });
});

server.listen(PORT, "0.0.0.0", function() {
  console.log('listening on *:' + PORT);
});
